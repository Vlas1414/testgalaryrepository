import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import {ListPhotosScreen} from '../screens/ListPhotosScreen';
import {PhotoScreen} from '../screens/PhotoScreen';

const ListPhotosNavigator = createStackNavigator();
function MyListPhotosNavigator() {
  return (
    <ListPhotosNavigator.Navigator>
      <ListPhotosNavigator.Screen
        name="ListPhotosScreen"
        component={ListPhotosScreen}
      />
    </ListPhotosNavigator.Navigator>
  );
}

const PhotoNavigator = createStackNavigator();
function MyPhotoNavigator() {
  return (
    <PhotoNavigator.Navigator>
      <PhotoNavigator.Screen name="PhotoScreen" component={PhotoScreen} />
    </PhotoNavigator.Navigator>
  );
}

const MainNavigator = createStackNavigator();
function MyMainNavigator() {
  return (
    <MainNavigator.Navigator initialRouteName="Gallery">
      <MainNavigator.Screen name="Gallery" component={MyListPhotosNavigator} />
      <MainNavigator.Screen name="Photo" component={MyPhotoNavigator} />
    </MainNavigator.Navigator>
  );
}
export const AppNavigation = () => {
  return (
    <NavigationContainer>
      <MyMainNavigator />
    </NavigationContainer>
  );
};
