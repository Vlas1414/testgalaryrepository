import React, {useEffect} from 'react';
import {View, StyleSheet, ActivityIndicator} from 'react-native';
import {FlatList, TouchableNativeFeedback} from 'react-native-gesture-handler';

import {PhotoCard} from '../components/PhotoCard';

import {useDispatch, useSelector} from 'react-redux';
import {loadPhotos, selectPhoto} from '../store/actions/photo';
import {FailLoading} from '../components/FailLoading';

export const ListPhotosScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const photos = useSelector((state) => state.photo.listPhotos);
  const loadingStatus = useSelector((state) => state.photo.loadingStatus);
  useEffect(() => {
    dispatch(loadPhotos());
  }, []);
  const cardPress = (id) => {
    navigation.navigate('Photo');
    dispatch(selectPhoto(id));
  };
  let content = (
    <ActivityIndicator size="large" color="gray" />
  );
  switch (loadingStatus) {
    case 'ready':
      content = (
        <FlatList
          keyExtractor={(item) => item.id.toString()}
          data={photos}
          renderItem={({item}) => (
            <TouchableNativeFeedback onPress={() => cardPress(item.id)}>
              <PhotoCard
                imgUri={item.links.download}
                user={item.user.name}
                desc={item.description}
                id={item.id}
              />
            </TouchableNativeFeedback>
          )}
        />
      );
      break;
    case 'fail':
      content = <FailLoading load={() => dispatch(loadPhotos())} />;
      break;
  }
  return <View style={styles.box}>{content}</View>;
};

const styles = StyleSheet.create({
  box: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
