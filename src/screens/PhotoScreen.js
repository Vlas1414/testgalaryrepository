import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {useSelector} from 'react-redux';

export const PhotoScreen = () => {
  const photoId = useSelector((state) => state.photo.selectPhotoId);
  const photo = useSelector((state) =>
    state.photo.listPhotos.filter((p) => p.id === photoId),
  )[0];
  const win = Dimensions.get('window');
  const ratio = win.width / photo.width; //541 is actual image width

  const [isImageReady, setIsImageReady] = useState(false);

  return (
    <ScrollView style={styles.box}>
      {isImageReady ? null : (
        <ActivityIndicator
          style={{
            ...styles.loading,
            ...{width: win.width, height: photo.height * ratio},
          }}
          size="large"
          color="gray"
        />
      )}
      <Image
        source={{
          uri: photo.links.download,
        }}
        style={{
          ...styles.image,
          ...{
            width: win.width,
            height: photo.height * ratio,
            marginTop: isImageReady ? 0 : -photo.height * ratio,
          },
        }}
        onLoadEnd={() => setIsImageReady(true)}
      />

      <View style={styles.container}>
        <View style={styles.rowContainer}>
          <Text style={styles.title}>Autor: </Text>
          <Text style={styles.value}>{photo.user.name}</Text>
        </View>
        {photo.description ? (
          <View style={{...styles.rowContainer, ...{flexDirection: 'column'}}}>
            <Text style={styles.title}>Description: </Text>
            <Text style={{...styles.value, ...{width: 325}}}>
              {photo.description}
            </Text>
          </View>
        ) : null}

        <View style={{...styles.rowContainer, ...{flexDirection: 'column'}}}>
          <Text style={styles.title}>Created at: </Text>
          <Text style={{...styles.value, ...{width: 325}}}>
            {photo.created_at}
          </Text>
        </View>
        <View style={styles.rowContainer}>
          <Text style={styles.title}>Likes: </Text>
          <Text style={styles.value}>{photo.likes}</Text>
        </View>
      </View>
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  box: {
    flex: 1,
  },
  container: {
    margin: 20,
    padding: 10,
    borderWidth: 1,
    borderRadius: 5,
  },
  rowContainer: {
    flexDirection: 'row',
  },
  title: {
    fontSize: 20,
    fontWeight: '700',
  },
  value: {
    fontSize: 20,
    fontFamily: 'Cochin',
    width: 230,
  },
  image: {
    width: '100%',
    height: 250,
    resizeMode: 'cover',
  },
  loading: {
    width: '100%',
    height: 250,
  },
});
