import {LOAD_PHOTOS, SELECT_PHOTO, CHANGE_STATUS_LOADING} from '../types';
import {Http} from '../../http';
import {Alert} from 'react-native';

export const loadPhotos = () => async (dispatch) => {
  try {
    dispatch({
      type: CHANGE_STATUS_LOADING,
      status: 'loading',
    });
    const listPhotos = await Http.get();
    dispatch({
      type: LOAD_PHOTOS,
      listPhotos,
    });
  } catch (ex) {
    console.log('Error http request: ', ex);
    if (ex.message === 'Network request failed') {
      await Alert.alert('Error ', 'No network access');
    }
    dispatch({
      type: CHANGE_STATUS_LOADING,
      status: 'fail',
    });
  }
};

export const selectPhoto = (id) => async (dispatch) => {
  dispatch({
    type: SELECT_PHOTO,
    photoId: id,
  });
};
