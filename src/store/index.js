import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {photoReducer} from './reducer/photo';

const rootReducer = combineReducers({
  photo: photoReducer,
});

export default createStore(rootReducer, applyMiddleware(thunk));
