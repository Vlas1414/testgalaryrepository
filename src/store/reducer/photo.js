import {LOAD_PHOTOS, SELECT_PHOTO, CHANGE_STATUS_LOADING} from '../types';

const initialState = {
  listPhotos: [],
  selectPhotoId: null,
  loadingStatus: 'loading',
};
export const photoReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_PHOTOS:
      return {
        ...state,
        listPhotos: action.listPhotos,
        loadingStatus: 'ready',
      };
    case CHANGE_STATUS_LOADING:
      return {
        ...state,
        loadingStatus: action.status,
      };
    case SELECT_PHOTO:
      return {
        ...state,
        selectPhotoId: action.photoId,
      };
    default:
      return state;
  }
};
