import React, {useState} from 'react';
import {View, Image, Text, StyleSheet, ActivityIndicator} from 'react-native';
export const PhotoCard = ({imgUri, user, desc}) => {
  const [isImageReady, setIsImageReady] = useState(false);
  return (
    <View style={styles.box}>
      {isImageReady ? null : (
        <ActivityIndicator style={styles.loading} size="large" color="gray" />
      )}
      <Image
        source={{
          uri: imgUri,
        }}
        style={{...styles.image, ...(isImageReady ? null : {marginLeft: -100})}}
        onLoadEnd={() => setIsImageReady(true)}
      />
      <View style={styles.textContainer}>
        <Text style={styles.autor}>{user}</Text>
        <Text style={styles.desc}>{desc}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  box: {
    flexDirection: 'row',
    margin: 10,
  },
  textContainer: {
    margin: 10,
  },
  autor: {
    fontWeight: '700',
  },
  desc: {
    width: 250,
  },
  image: {
    width: 100,
    height: 100,
    borderRadius: 10,
  },
  loading: {
    width: 100,
    height: 100,
  },
});
