export class Http {
  static URL = 'https://api.unsplash.com/photos';
  static client_id =
    'cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0';

  static async get() {
    try {
      const config = {
        method: 'GET',
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      };
      const link = `${Http.URL}?client_id=${Http.client_id}`;
      const response = await fetch(link, config);
      return await response.json();
    } catch (ex) {
      throw ex;
    }
  }
}
